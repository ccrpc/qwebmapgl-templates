# QGIS Web Map Templates
Templates for creating web maps from QGIS using the [Web Map
Exporter](https://gitlab.com/ccrpc/qwebmapgl).

## License
QGIS Web Map Templates is available under the terms of the [BSD 3-clause
license](https://gitlab.com/ccrpc/qwebmapgl-templates/blob/master/LICENSE.md).
