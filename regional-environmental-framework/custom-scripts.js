// import * as pdfFonts from "{{ options.base_url }}/vfs_fonts.js";
// import * as pdfMake from "{{ options.base_url }}/pdfmake.js";
// import * as pdfFonts from "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.72/vfs_fonts.js";
// import * as pdfMake from "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.72/pdfmake.min.js";
pdfMake.vfs = pdfFonts;

const queryFeatureSource = {
  type: "geojson",
  data: {
    type: "Feature",
    geometry: {
      type: "Polygon",
      coordinates: [[[0, 0]]],
    },
  },
};
const queryFeatureLine = {
  id: "spatialQueryLine",
  type: "line",
  source: "spatialQuery",
  paint: {
    "line-color": "#ffaf33",
    "line-width": 2,
  },
  layout: {
    visibility: "visible",
  },
};

const queryFeatureFill = {
  id: "spatialQueryFill",
  type: "fill",
  source: "spatialQuery",
  paint: {
    "fill-color": "rgba(56, 135, 190, 0.1)",
  },
  layout: {
    visibility: "visible",
  },
};

function uniqueArray(array) {
  let obj = {};

  array.forEach(function (element) {
    const str = JSON.stringify(element);
    if (!obj[str]) {
      obj[str] = element;
    }
  });

  return Object.keys(obj).map((v) => obj[v]);
}

function roundIfNumber(number, decimals) {
  if (typeof number !== "number") return number;
  return Number(Math.round(number + "e" + decimals) + "e-" + decimals);
}

function paddingTD(layerCol, maxCol) {
  if (layerCol == maxCol) {
    return "";
  } else if (layerCol > maxCol) {
    throw Error(`LayerCol (${layerCol}) is greater than maxCol ${maxCol}`);
  } else {
    return `<td class="padding" colspan="${maxCol - layerCol}">`;
  }
}

function displayHeaderRow(featureDict, layer, maxCol) {
  let html = '<tr class="header-row">';

  let layerCol = 0;
  for (let i = 0; i < featureDict[layer]["header"].length; i++) {
    html += `
      <th>${featureDict[layer]["header"][i]}</th>
      `;
    layerCol += 1;
  }

  html += paddingTD(layerCol, maxCol);
  html += "</tr>";
  return html;
}

function createFeatureDict(features) {
  let featureDict = {};
  for (let i = 0; i < features.length; i++) {
    if (!featureDict[features[i].source]) {
      featureDict[features[i].source] = [];
    }
    featureDict[features[i].source].push(features[i].properties);
  }

  let metaDict = {};

  let maxCol = 0;
  for (let layer in featureDict) {
    if (Object.prototype.hasOwnProperty.call(featureDict, layer)) {
      if (!featureDict[layer]["header"]) {
        featureDict[layer]["header"] = [];
      }
      if (!metaDict[layer]) {
        metaDict[layer] = {};
      }
      for (let i = 0; i < featureDict[layer].length; i++) {
        let feature = featureDict[layer][i];
        for (let field in feature) {
          if (Object.prototype.hasOwnProperty.call(feature, field)) {
            if (!featureDict[layer]["header"].includes(field)) {
              featureDict[layer]["header"].push(field);
            }
          }
        }
      }
      metaDict[layer]["maxLayerCol"] = featureDict[layer]["header"].length;
      if (metaDict[layer]["maxLayerCol"] > maxCol) {
        maxCol = metaDict[layer]["maxLayerCol"];
      }
    }
  }

  metaDict["maxCol"] = maxCol;

  console.log(featureDict);
  console.log(metaDict);
  return [featureDict, metaDict];
}

function buildDrawerHTML(features) {
  if (features.length === 0) {
    return `
      <tr class="header-row">
        <th>No Results in Search Area</th>
      </tr>`;
  }

  let [featureDict, metaDict] = createFeatureDict(features);
  let maxCol = metaDict["maxCol"];

  let html = "";

  for (let layer in featureDict) {
    if (Object.prototype.hasOwnProperty.call(featureDict, layer)) {
      html += `
        <tr class="new-section">
          <th colspan="${maxCol}">
            ${layer}
          </th>
        </tr>`;

      html += displayHeaderRow(featureDict, layer, maxCol);

      for (let i = 0; i < featureDict[layer].length; i++) {
        let layerCol = 0;
        html += "<tr>";
        let feature = featureDict[layer][i];
        for (let property in feature) {
          if (Object.prototype.hasOwnProperty.call(feature, property)) {
            html += `
              <td>${roundIfNumber(feature[property], 2)}</td>
            `;
            layerCol += 1;
          }
        }
        html += paddingTD(layerCol, maxCol);
        html += "</tr>";
      }
    }
  }
  return html;
}

function emptyPDFColumns(number) {
  let empty = [];
  for (let i = 0; i < number; i++) {
    empty.push("");
  }
  return empty;
}

function paddingPDF(number, style) {
  let padding = [];
  if (number != 0) {
    padding.push({
      text: "",
      style: style,
      colSpan: number,
      border: [false, false, true, false],
    });
  }
  const empty = emptyPDFColumns(number - 1);
  padding = padding.concat(empty);
  return padding;
}

function buildResultPDF(features, imageURL) {
  let pdfDefinition = {
    pageSize: "LETTER",
    pageOrientation: "portrait",
    pageMargins: 20,
    styles: {
      layerHeader: {
        alignment: "center",
      },
      newLayer: {
        alignment: "center",
      },
      featureAttribute: {
        alignment: "left",
      },
      padding: {
        alignment: "center",
      },
      layerTable: {
        margin: [0, 2, 0, 2],
      },
      title: {
        alignment: "center",
        bold: true,
        fontSize: 17,
      },
    },
    footer: [
      {
        svg: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 771 320"><title>Artboard 1</title><circle cx="159.38" cy="220.87" r="54.63" fill="none" stroke="#007369" stroke-miterlimit="10" stroke-width="6"/><line x1="214" y1="220.88" x2="214" y2="315.88" fill="none" stroke="#007369" stroke-miterlimit="10" stroke-width="6"/><circle cx="159.38" cy="97.01" r="54.63" fill="none" stroke="#007369" stroke-miterlimit="10" stroke-width="6"/><line x1="104.75" y1="97" x2="104.75" y2="2" fill="none" stroke="#007369" stroke-miterlimit="10" stroke-width="6"/><circle cx="97.57" cy="158.94" r="54.63" fill="none" stroke="#007369" stroke-miterlimit="10" stroke-width="6"/><line x1="97.56" y1="213.57" x2="2.56" y2="213.57" fill="none" stroke="#007369" stroke-miterlimit="10" stroke-width="6"/><path d="M267.8,120.9l-8.5,6.52a46.46,46.46,0,0,0-16.88-13.88,49.4,49.4,0,0,0-21.64-4.72A47.84,47.84,0,0,0,196.91,115a44.43,44.43,0,0,0-17,16.63,46,46,0,0,0-6,23.47q0,19.7,13.51,32.89t34.09,13.18q22.63,0,37.87-17.72l8.5,6.45a54.29,54.29,0,0,1-20.1,15.86,63.08,63.08,0,0,1-26.92,5.6q-28.27,0-44.6-18.82-13.7-15.89-13.7-38.38,0-23.66,16.59-39.81T220.64,98.2a61,61,0,0,1,27.25,6A52.9,52.9,0,0,1,267.8,120.9Z"/><path d="M289.78,100.91h10.77v65a134.53,134.53,0,0,0,.44,14.43A23.93,23.93,0,0,0,304.72,191a22,22,0,0,0,9,7.18A28.21,28.21,0,0,0,326,201.11a24.13,24.13,0,0,0,10.25-2.27,22.78,22.78,0,0,0,8.2-6.3,25.69,25.69,0,0,0,4.83-9.74q1.1-4.1,1.1-16.85v-65h10.77v65q0,14.43-2.82,23.33T347,204.77q-8.46,6.59-20.47,6.59-13,0-22.34-6.23a30,30,0,0,1-12.45-16.48q-2-6.3-2-22.71Z"/><path d="M388,100.91h10.77v65a134.53,134.53,0,0,0,.44,14.43A23.93,23.93,0,0,0,402.94,191a22,22,0,0,0,9,7.18,28.21,28.21,0,0,0,12.23,2.89,24.13,24.13,0,0,0,10.25-2.27,22.78,22.78,0,0,0,8.2-6.3,25.69,25.69,0,0,0,4.83-9.74q1.1-4.1,1.1-16.85v-65h10.77v65q0,14.43-2.82,23.33t-11.28,15.49q-8.46,6.59-20.47,6.59-13,0-22.34-6.23A30,30,0,0,1,390,188.65q-2-6.3-2-22.71Z"/><path d="M529.87,100.91l50.24,107.74H568.48L551.53,173.2H505.12l-16.78,35.45h-12l50.9-107.74Zm-1.36,22.89L510,162.8h36.83Z"/><path d="M574,111.46V100.91h59v10.55H609v97.19H598V111.46Z"/><path d="M637.38,188.58l9.16-5.49q9.67,17.8,22.34,17.8a21.4,21.4,0,0,0,10.18-2.53,17.88,17.88,0,0,0,7.25-6.77,17.54,17.54,0,0,0,2.49-9A18.26,18.26,0,0,0,685.14,172q-5.05-7.18-18.46-17.29t-16.77-14.72a26.91,26.91,0,0,1-5.71-16.48,25,25,0,0,1,3.37-12.82,24,24,0,0,1,9.48-9.12,27.34,27.34,0,0,1,13.29-3.33A28.34,28.34,0,0,1,684.59,102q6.63,3.77,14,13.88l-8.79,6.67q-6.08-8.06-10.36-10.62a17.86,17.86,0,0,0-9.34-2.56,14.79,14.79,0,0,0-10.66,4,12.94,12.94,0,0,0-4.14,9.74,16.59,16.59,0,0,0,1.46,6.81,25.11,25.11,0,0,0,5.35,7.18q2.12,2.05,13.92,10.77,14,10.33,19.19,18.38a29.63,29.63,0,0,1,5.2,16.19q0,11.72-8.9,20.36a29.91,29.91,0,0,1-21.64,8.64,31.75,31.75,0,0,1-17.8-5.24Q644.12,200.89,637.38,188.58Z"/></svg>',
        width: 45,
        alignment: "right",
      },
    ],
    defaultStyle: {
      alignment: "left",
    },
    content: [
      {
        text: "Champaign County Regional Environmental Framework",
        style: "title",
      },
      {
        image: imageURL,
        width: 572,
      },
    ],
  };

  let [featureDict, metaDict] = createFeatureDict(features);
  const maxCol = metaDict["maxCol"];

  for (let layer in featureDict) {
    if (Object.prototype.hasOwnProperty.call(featureDict, layer)) {
      // Define Table
      let table = {
        widths: [],
        body: [],
        headerRows: 2,
      };
      let maxLayerCol = metaDict[layer]["maxLayerCol"];
      const percentSize = Math.floor((100 / maxLayerCol) * 100) / 100;

      for (let i = 0; i < maxLayerCol - 1; i++) {
        table.widths.push(`${percentSize}%`);
      }
      table.widths.push(`${100 - (maxLayerCol - 1) * percentSize}%`);

      // New Layer \ Table
      const newLayer = [
        {
          text: layer,
          style: "newLayer",
          colSpan: maxLayerCol,
          border: [true, true, true, false],
        },
      ];
      let empty = emptyPDFColumns(maxLayerCol - 1);
      table.body.push(newLayer.concat(empty));

      // Layer Header
      let layerHeader = [];
      let layerCol = 0;
      for (let i = 0; i < featureDict[layer]["header"].length; i++) {
        const header = featureDict[layer]["header"][i];
        layerHeader.push({
          text: header,
          style: "layerHeader",
          border: [true, false, true, false],
        });
        layerCol += 1;
      }
      const padding = paddingPDF(maxLayerCol - layerCol, "padding");
      table.body.push(layerHeader.concat(padding));

      // Attribute Rows
      for (let i = 0; i < featureDict[layer].length; i++) {
        let attributeRow = [];
        let layerCol = 0;
        let feature = featureDict[layer][i];
        let lastRow = i == featureDict[layer].length - 1;
        let lastCol;
        for (let field in feature) {
          if (Object.prototype.hasOwnProperty.call(feature, field)) {
            layerCol += 1;
            lastCol = layerCol === maxLayerCol;
            attributeRow.push({
              text: roundIfNumber(feature[field], 2),
              style: "featureAttribute",
              border: [true, false, lastCol, lastRow],
            });
          }
        }
        const padding = paddingPDF(maxLayerCol - layerCol, "padding");
        if (lastRow && padding.length !== 0) {
          padding[0].border[3] = true;
        }
        table.body.push(attributeRow.concat(padding));
      }
      // Push this table
      const contentItem = {
        table: table,
        style: "layerTable",
        layout: {
          fillColor: function (rowIndex, node, columnIndex) {
            return rowIndex % 2 === 0 ? "#f2f2f2" : "#ddd";
          },
          vLineColor: function (columnIndex, node, rowIndex) {
            if (columnIndex === 0 || columnIndex === node.table.widths.length) {
              return "black";
            } else {
              return rowIndex % 2 === 0 ? "#ddd" : "#f2f2f2";
            }
          },
          hLineColor: function (lineIndex, node, i) {
            if (lineIndex == 0 || lineIndex == node.table.body.length) {
              return "black";
            } else {
              return i % 2 === 0 ? "#f2f2f2" : "#ddd";
            }
          },
        },
      };
      pdfDefinition.content.push(contentItem);
    }
  }

  console.log(pdfDefinition);
  return pdfDefinition;
}

function takeScreenshot(map) {
  return new Promise((resolve, reject) => {
    map.once("render", () => {
      resolve(map.getCanvas().toDataURL());
    });
    map._render();
  });
}

function exportPDF(features, imgURLPromise) {
  console.log("exportPDF called");

  imgURLPromise
    .then((url) => buildResultPDF(features, url))
    .then((definition) => pdfMake.createPdf(definition))
    .then((pdf) => pdf.download());
}

async function enableSpacialQuery() {
  await customElements.whenDefined("gl-map");
  const mapEl = document.querySelector("gl-map");
  if (!mapEl) {
    console.log("Map Element not Found");
    return;
  }
  await mapEl.componentOnReady();
  const map = mapEl.map;

  map.once("render", () => {
    console.log("Triggered on Map Render, inserting functionality");

    const drawer = document.getElementById("resultDrawer");
    mapEl.style.display = "block";

    const canvasContainer = map.getCanvasContainer();

    // The start, end, and box containers for the selection.
    let start;
    let current;
    let box;
    let featureSource = false;
    let exportListener = undefined;
    let nameMapping = undefined;
    const searchToggle = document.getElementById("searchToggle");
    const searchBar = document.getElementById("searchBar");

    const scaleImperial = new mapboxgl.ScaleControl({
      maxWidth: 150,
      unit: "imperial",
    });
    const scaleMetric = new mapboxgl.ScaleControl({
      maxWidth: 150,
      unit: "metric",
    });
    const nav = new mapboxgl.NavigationControl({
      showCompass: true,
      showZoom: false,
      visualizePitch: false,
    });

    map.addControl(scaleImperial);
    map.addControl(scaleMetric);
    map.addControl(nav, "top-right");

    map.boxZoom.disable();
    map.setMaxPitch(0);
    canvasContainer.addEventListener("mousedown", mouseDown, true);
    document.addEventListener("glStyleUpdated", glStyleUpdated, true);
    searchToggle.addEventListener(
      "click",
      () =>
        (searchBar.style.display =
          searchBar.style.display === "block" ? "none" : "block")
    );

    fetch("./data/name-mapping.json")
      .then((response) => response.json())
      .then((json) => {
        nameMapping = json;
        console.log("Name Mapping Finalized");
      });

    function updateExport(newFeatures) {
      console.log("Updating state of Export Listener");
      if (newFeatures) {
        if (exportListener) {
          updateExport(undefined);
          updateExport(newFeatures);
        } else {
          exportListener = () => exportPDF(newFeatures, takeScreenshot(map));
          drawer
            .querySelector("ion-button[title=Export]")
            .addEventListener("click", exportListener);
          console.log("Export Listener Added");
        }
      } else if (exportListener !== undefined) {
        drawer
          .querySelector("ion-button[title=Export]")
          .removeEventListener("click", exportListener);
        exportListener = undefined;
        console.log("Export Listener Removed");
      }
    }

    function glStyleUpdated() {
      console.log("Gl Style Updated");
      featureSource = false;
      if (drawer.open) {
        const resultTable = document.getElementById("resultTable");
        resultTable.innerHTML = `
        <tr>
          <th>Query Data is Loading</th>
        </tr>`;

        closeDrawer();
      }
    }

    // Return the xy coordinates of the mouse position
    function mousePos(e) {
      const rect = canvasContainer.getBoundingClientRect();
      return new mapboxgl.Point(
        e.clientX - rect.left - canvasContainer.clientLeft,
        e.clientY - rect.top - canvasContainer.clientTop
      );
    }

    function mouseDown(e) {
      // Continue the rest of the function if the shiftkey is pressed.
      if (!(e.shiftKey && e.button === 0)) return;

      // Disable moving the map when the shift key is held down.
      map.dragPan.disable();

      // Call functions for the following events
      document.addEventListener("mousemove", onMouseMove);
      document.addEventListener("mouseup", onMouseUp);
      document.addEventListener("keydown", onKeyDown);

      // Capture the first xy coordinates
      start = mousePos(e);
    }

    function onMouseMove(e) {
      // Capture the ongoing xy coordinates
      current = mousePos(e);

      // Append the box element if it does not exist
      if (!box) {
        box = document.createElement("div");
        box.classList.add("boxdraw");
        canvasContainer.appendChild(box);
      }

      const minX = Math.min(start.x, current.x),
        maxX = Math.max(start.x, current.x),
        minY = Math.min(start.y, current.y),
        maxY = Math.max(start.y, current.y);

      // Adjust width and xy position of the box element ongoing
      const pos = `translate(${minX}px, ${minY}px)`;
      box.style.transform = pos;
      box.style.width = maxX - minX + "px";
      box.style.height = maxY - minY + "px";
    }

    function onMouseUp(e) {
      // Capture xy coordinates
      finish([start, mousePos(e)]);
    }

    function onKeyDown(e) {
      // If the ESC key is pressed
      if (e.keyCode === 27) finish();
    }

    function populateDrawer(featuresDisplay) {
      const resultTable = document.getElementById("resultTable");
      resultTable.innerHTML = `
        <tr>
          <th>Query Data is Loading</th>
        </tr>`;
      resultTable.innerHTML = buildDrawerHTML(featuresDisplay);
    }

    function drawBox(unProjectedBox) {
      const coordinates = [
        [unProjectedBox[0].lng, unProjectedBox[0].lat],
        [unProjectedBox[1].lng, unProjectedBox[0].lat],
        [unProjectedBox[1].lng, unProjectedBox[1].lat],
        [unProjectedBox[0].lng, unProjectedBox[1].lat],
        [unProjectedBox[0].lng, unProjectedBox[0].lat],
      ];

      queryFeatureSource.data.geometry.coordinates[0] = coordinates;
      if (!featureSource) {
        map.addSource("spatialQuery", queryFeatureSource);
        map.addLayer(queryFeatureFill);
        map.addLayer(queryFeatureLine);
        featureSource = true;
      }
      map.getSource("spatialQuery").setData(queryFeatureSource.data);
    }

    function openDrawer(unProjectedBox, featuresDisplay) {
      if (drawer.open) {
        return;
      }

      drawer.toggle();
      const bounds = map.getBounds();
      const center = bounds.getCenter();
      const latAverage = (center.lat + bounds.getNorth()) / 2;
      const latOffset = latAverage - center.lat;

      const bboxCenter = [
        (unProjectedBox[0].lng + unProjectedBox[1].lng) / 2,
        (unProjectedBox[0].lat + unProjectedBox[1].lat) / 2 - latOffset,
      ];
      map.easeTo({
        center: bboxCenter,
        duration: 1500,
      });
      drawer
        .querySelector("ion-button[title=Close]")
        .addEventListener("click", closeDrawer);
    }

    function closeDrawer() {
      if (drawer.open) {
        drawer
          .querySelector("ion-button[title=Close]")
          .removeEventListener("click", closeDrawer);

        updateExport(undefined);

        removeBox();
        drawer.toggle();
      }
    }

    function removeBox() {
      if (!featureSource) {
        return;
      }
      queryFeatureSource.data.geometry.coordinates = [[[0, 0]]];
      map.getSource("spatialQuery").setData(queryFeatureSource.data);
    }

    function finish(bbox) {
      // Remove these events now that finish has been called.
      document.removeEventListener("mousemove", onMouseMove);
      document.removeEventListener("keydown", onKeyDown);
      document.removeEventListener("mouseup", onMouseUp);

      // If bbox exists. use this value as the argument for `queryRenderedFeatures`
      if (bbox) {
        const features = map
          .queryRenderedFeatures(bbox)
          .filter((feature) => feature.source && feature.source == "map:data");
        // if (features.length >= 1000) {
        //   return window.alert("Select a smaller number of features");
        // }

        const featuresDisplay = features.map((feature) => {
          let source;
          if (feature.sourceLayer) {
            if (nameMapping && nameMapping[feature.sourceLayer]) {
              source = nameMapping[feature.sourceLayer];
            } else {
              source = feature.sourceLayer;
            }
          } else {
            source = "Not Defined";
          }

          const display = {
            source: source,
            properties: {
              ...feature.properties,
            },
          };
          display.toJSON = feature.toJSON;
          return display;
        });

        const uniqueDisplayFeatures = uniqueArray(featuresDisplay);

        console.log(uniqueDisplayFeatures);

        // Unproject the bbox so we have it's mapbox gl coordinates
        let unProjectedBox = new Array(2);
        unProjectedBox[0] = map.unproject(bbox[0]);
        unProjectedBox[1] = map.unproject(bbox[1]);
        drawBox(unProjectedBox);
        openDrawer(unProjectedBox, uniqueDisplayFeatures);

        populateDrawer(uniqueDisplayFeatures);
        updateExport(uniqueDisplayFeatures);

        // const vectorLayers = map.getSource("map:data").vectorLayers;
        // let sourceFeatures = {};
        // for (let vectorLayer of vectorLayers) {
        //   if (!sourceFeatures[vectorLayer]) {
        //     sourceFeatures[vectorLayer] = [];
        //   }
        //   sourceFeatures[vectorLayer] = sourceFeatures[vectorLayer].concat(
        //     map.querySourceFeatures("map:data", { sourceLayer: vectorLayer.id })
        //   );
        // }
        // console.log(sourceFeatures);
      }

      map.dragPan.enable();

      if (box) {
        box.parentNode.removeChild(box);
        box = null;
      }
    }
  });
  map._render();
}

console.log("Custom Script Loaded");
enableSpacialQuery();
