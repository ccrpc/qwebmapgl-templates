# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## [0.4.3](https://gitlab.com/ccrpc/qwebmapgl-templates/compare/v0.4.2...v0.4.3) (2024-11-21)


### Features

* Switching to jsDelivr ([abe49d2](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/abe49d2f4d8547a2c2e2c375a88b5d785da07c97))

## [0.4.2](https://gitlab.com/ccrpc/qwebmapgl-templates/compare/v0.4.1...v0.4.2) (2024-08-28)


### Features

* Updating TIP ([e786fde](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/e786fdecb932bc5d94c87528b557e939a7f23130))

## [0.4.1](https://gitlab.com/ccrpc/qwebmapgl-templates/compare/v0.4.0...v0.4.1) (2024-08-27)


### Features

* Updating tip version ([343e8ab](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/343e8ab36990800fe379e3606ef026bd19042f32))
* Updating webmapgl version ([c47caf8](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/c47caf832b2bec80c932c7b97c123fa1b6ff3cb5))

## [0.4.0](https://gitlab.com/ccrpc/qwebmapgl-templates/compare/v0.3.8...v0.4.0) (2024-01-16)


### Features

* Attributes show in popup ([3c88a73](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/3c88a73e8198543d95d69ddba3f2d07ffbae7d06))
* Border and fit-content ([bdf0631](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/bdf063156375e2a3a821ba44cc000573e898a367))
* Box and modal only close when you click the canvas ([d4907d8](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/d4907d8237977139be9296016e71c6716f3a2b15))
* Box now styled ([d175778](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/d17577834ca862a6c18c2b8029f41b40f11d0a8e))
* Centering solid ([3fb6717](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/3fb67178a5ee6bc37d93cf848cfe5563f63efe0e))
* Initial ref commit ([5494c9f](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/5494c9fe49f1b2480dc7282630fa0161878421d0))
* injecting into webmap ([1c6b545](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/1c6b54558ca89f63fc81a7f0347ca9d448ef715b))
* Map does not show till init, disable pitch ([1c16a0e](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/1c16a0e8a42452f0c8e41a397be9ad4a5c34c581))
* Margins work ([d1db161](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/d1db161ed6efb0960bad2cf7a290cd0313a0a6ab))
* Modal at right and informs if no results ([4883e90](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/4883e906bea91a225efaf0bf0a9843f691c2eb9b))
* Mouse rotate matches keyboard ([f88e09d](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/f88e09df5dead4e2b4a2060c244ec3cfdbec98b8))
* No zoom with modal open ([74a5666](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/74a5666ebf3742b3da3f0f836772daae05da2174))
* Popup opens in center of selected box ([6815bf5](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/6815bf5bb78ab037b7312eaa0bc7029ec5342a92))
* querysourcefeatures for viewport ([00825e1](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/00825e11096254c9c3eae04077099d28a61fe30e))
* **REF:** Added address search ([76f0f22](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/76f0f227ebc7abe257ded21bf6ade5d5778efd1a))
* **REF:** Added logo ([ebb8659](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/ebb86591dac77ef0314cbd4b8af28c1f81b2f8b5))
* **REF:** Altering line color for visibility ([aef35ca](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/aef35caf1b0f12ab44dfc6d10f3b77a24f96d38b))
* **REF:** BBox draws on map ([bc6046e](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/bc6046ee65103379cfeb32d7cd385ec40b4adfc8))
* **REF:** Border Colors ([bdaf524](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/bdaf524e98d7345bf48544156bec5b625c8b2855))
* **REF:** Borders are defined ([caab586](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/caab5864d99647469c9b954cfabf2a7e03910c0e))
* **REF:** Canvas bbox removed as openmap gl feature drawn ([708a393](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/708a3937ac8d0cc9c6a2398b7099eff81ce74dd1))
* **REF:** Capped search bar width ([2629419](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/26294195aeba356889913cc3e59370c367bdcf9a))
* **REF:** Close button more accurate ([6c0f103](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/6c0f1036fd2752e568930aec2337ebd75b46f6cc))
* **REF:** Closing drawer now removes bounding box ([47c92c5](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/47c92c5ff83615bd59c7d4aba55c25cd36ca3f26))
* **REF:** Drawer Export Button Correct ([718509d](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/718509daa4c5216b1893e62fd2a4d3eda0a73826))
* **REF:** Export button exists and triggers function ([769b290](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/769b2909940eafa42b621695d5ca6651266fc31f))
* **REF:** Feature details now displayed in drawer ([a4190fc](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/a4190fc13270df8ed170a4f40b77b5b24c1797ed))
* **REF:** Header Row Created ([c2e2366](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/c2e236670842c3ebe6dba9f2d616b832bdfb6b6e))
* **REF:** Header Row has correct features ([61a18c8](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/61a18c8cd398aeab1089350086553e5cb77897b4))
* **REF:** Listening to the map updated event ([0a9fc8c](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/0a9fc8c310339bf07c03d54568736cbcacfa98a1))
* **REF:** Map loading more robust ([1ead263](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/1ead2635271047c92cc0742a8bbc6257f10dae70))
* **REF:** Map zooms when drawer opened ([c40ffcb](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/c40ffcba465a051894d34da995ad61cc0579d52f))
* **REF:** Minor Cosmetic Changes ([cc65d7a](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/cc65d7a0b8acba16ea1d094bedf8b2028fd366cb))
* **REF:** Navigation and scale styling ([2493d55](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/2493d559de1066ee6692872e74b2d4afb059667c))
* **REF:** Outline for export ([299bd3b](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/299bd3bfdbc501c695b7d0cff8ae7455e73eae29))
* **REF:** Pdf now includes image ([c98e13b](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/c98e13bc0dd8b68de33a52077b59efc6dc84907b))
* **REF:** Query now added once on render ([d78389e](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/d78389e09fbb735e28eb0fc02caf39af38865bd6))
* **ref:** Query results now displayed in Alert ([7467656](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/746765662083b38568d6de872a5ef5eb6af762e1))
* **REF:** Removed duplicate entries from spacial result ([87b8334](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/87b8334459e6aae53f4ee69567bcfa8e88bd662b))
* **ref:** result excludes basemap features ([d6a512f](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/d6a512f2b032391210b6acf8e8e95395b7e5054f))
* **REF:** Scale and Nav control ([be2c69f](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/be2c69f150453beeec4190c59c5734a94891437c))
* **REF:** Search bar toggles ([b666e2a](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/b666e2ad39b02546a74deae6dca9c86f4a312655))
* **REF:** Separate tables with header rows ([4a2fe6e](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/4a2fe6ed811fc9b899997ba2649992f621603c27))
* **REF:** Source id maped to names ([8905456](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/8905456b93c58a204146e97b1c02e62be7e929ad))
* **REF:** Spatial Search Opens Drawer ([3976b2a](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/3976b2ad4cdc025fc4a20cc78cda6b1bd1e90dc0))
* **REF:** Strict mode and start of pdfmake ([7270679](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/7270679d018e6200cd8503744419d8916c1e21fb))
* **REF:** Table definition and expand ([1d06920](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/1d06920e185ca2927e0218a4f142fcba7c983393))
* **REF:** The basics of pdf export ([77d3f04](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/77d3f04f650f922f03a95a62129ba2735b918026))
* **REF:** Using percentages for pdfmake tables ([9608a26](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/9608a260ff61a83bcb370cf33717af4125e1621e))
* Result vertically centered ([8ab630d](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/8ab630d13a61ccde91008fe536e56c2b35ebf58c))
* Rounding to two decimal places ([5d8e9cd](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/5d8e9cd813d5a413f3693d9b789679f6efde9731))
* Starting on layer features ([111ce35](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/111ce3526a24fe92e58094a35696ce0be2e7bffd))
* Style now in own css and js drawing box ([c93f744](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/c93f7448024c675393e272703c4b6ade16ff8b44))
* Visible Modal Div ([4491081](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/44910812c5e1c835098a4c87c0cd48bd6b513d49))
* Zoom disable now waits for map to load ([15ae5bf](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/15ae5bfa397fecbea0c0683c42a5a8d458ea64f0))


### Bug Fixes

* **REF:** fixed function name mispelling ([13d31af](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/13d31aff3a2868e2a5e7e40e44e84fc27cb464d3))

## [0.4.0](https://gitlab.com/ccrpc/qwebmapgl-templates/compare/v0.3.8...v0.4.0) (2024-01-16)


### Features

* Attributes show in popup ([3c88a73](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/3c88a73e8198543d95d69ddba3f2d07ffbae7d06))
* Border and fit-content ([bdf0631](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/bdf063156375e2a3a821ba44cc000573e898a367))
* Box and modal only close when you click the canvas ([d4907d8](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/d4907d8237977139be9296016e71c6716f3a2b15))
* Box now styled ([d175778](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/d17577834ca862a6c18c2b8029f41b40f11d0a8e))
* Centering solid ([3fb6717](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/3fb67178a5ee6bc37d93cf848cfe5563f63efe0e))
* Initial ref commit ([5494c9f](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/5494c9fe49f1b2480dc7282630fa0161878421d0))
* injecting into webmap ([1c6b545](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/1c6b54558ca89f63fc81a7f0347ca9d448ef715b))
* Map does not show till init, disable pitch ([1c16a0e](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/1c16a0e8a42452f0c8e41a397be9ad4a5c34c581))
* Margins work ([d1db161](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/d1db161ed6efb0960bad2cf7a290cd0313a0a6ab))
* Modal at right and informs if no results ([4883e90](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/4883e906bea91a225efaf0bf0a9843f691c2eb9b))
* Mouse rotate matches keyboard ([f88e09d](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/f88e09df5dead4e2b4a2060c244ec3cfdbec98b8))
* No zoom with modal open ([74a5666](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/74a5666ebf3742b3da3f0f836772daae05da2174))
* Popup opens in center of selected box ([6815bf5](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/6815bf5bb78ab037b7312eaa0bc7029ec5342a92))
* querysourcefeatures for viewport ([00825e1](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/00825e11096254c9c3eae04077099d28a61fe30e))
* **REF:** Added address search ([76f0f22](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/76f0f227ebc7abe257ded21bf6ade5d5778efd1a))
* **REF:** Added logo ([ebb8659](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/ebb86591dac77ef0314cbd4b8af28c1f81b2f8b5))
* **REF:** Altering line color for visibility ([aef35ca](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/aef35caf1b0f12ab44dfc6d10f3b77a24f96d38b))
* **REF:** BBox draws on map ([bc6046e](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/bc6046ee65103379cfeb32d7cd385ec40b4adfc8))
* **REF:** Border Colors ([bdaf524](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/bdaf524e98d7345bf48544156bec5b625c8b2855))
* **REF:** Borders are defined ([caab586](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/caab5864d99647469c9b954cfabf2a7e03910c0e))
* **REF:** Canvas bbox removed as openmap gl feature drawn ([708a393](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/708a3937ac8d0cc9c6a2398b7099eff81ce74dd1))
* **REF:** Capped search bar width ([2629419](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/26294195aeba356889913cc3e59370c367bdcf9a))
* **REF:** Close button more accurate ([6c0f103](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/6c0f1036fd2752e568930aec2337ebd75b46f6cc))
* **REF:** Closing drawer now removes bounding box ([47c92c5](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/47c92c5ff83615bd59c7d4aba55c25cd36ca3f26))
* **REF:** Drawer Export Button Correct ([718509d](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/718509daa4c5216b1893e62fd2a4d3eda0a73826))
* **REF:** Export button exists and triggers function ([769b290](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/769b2909940eafa42b621695d5ca6651266fc31f))
* **REF:** Feature details now displayed in drawer ([a4190fc](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/a4190fc13270df8ed170a4f40b77b5b24c1797ed))
* **REF:** Header Row Created ([c2e2366](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/c2e236670842c3ebe6dba9f2d616b832bdfb6b6e))
* **REF:** Header Row has correct features ([61a18c8](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/61a18c8cd398aeab1089350086553e5cb77897b4))
* **REF:** Listening to the map updated event ([0a9fc8c](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/0a9fc8c310339bf07c03d54568736cbcacfa98a1))
* **REF:** Map loading more robust ([1ead263](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/1ead2635271047c92cc0742a8bbc6257f10dae70))
* **REF:** Map zooms when drawer opened ([c40ffcb](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/c40ffcba465a051894d34da995ad61cc0579d52f))
* **REF:** Minor Cosmetic Changes ([cc65d7a](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/cc65d7a0b8acba16ea1d094bedf8b2028fd366cb))
* **REF:** Navigation and scale styling ([2493d55](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/2493d559de1066ee6692872e74b2d4afb059667c))
* **REF:** Outline for export ([299bd3b](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/299bd3bfdbc501c695b7d0cff8ae7455e73eae29))
* **REF:** Pdf now includes image ([c98e13b](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/c98e13bc0dd8b68de33a52077b59efc6dc84907b))
* **REF:** Query now added once on render ([d78389e](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/d78389e09fbb735e28eb0fc02caf39af38865bd6))
* **ref:** Query results now displayed in Alert ([7467656](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/746765662083b38568d6de872a5ef5eb6af762e1))
* **REF:** Removed duplicate entries from spacial result ([87b8334](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/87b8334459e6aae53f4ee69567bcfa8e88bd662b))
* **ref:** result excludes basemap features ([d6a512f](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/d6a512f2b032391210b6acf8e8e95395b7e5054f))
* **REF:** Scale and Nav control ([be2c69f](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/be2c69f150453beeec4190c59c5734a94891437c))
* **REF:** Search bar toggles ([b666e2a](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/b666e2ad39b02546a74deae6dca9c86f4a312655))
* **REF:** Separate tables with header rows ([4a2fe6e](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/4a2fe6ed811fc9b899997ba2649992f621603c27))
* **REF:** Source id maped to names ([8905456](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/8905456b93c58a204146e97b1c02e62be7e929ad))
* **REF:** Spatial Search Opens Drawer ([3976b2a](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/3976b2ad4cdc025fc4a20cc78cda6b1bd1e90dc0))
* **REF:** Strict mode and start of pdfmake ([7270679](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/7270679d018e6200cd8503744419d8916c1e21fb))
* **REF:** Table definition and expand ([1d06920](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/1d06920e185ca2927e0218a4f142fcba7c983393))
* **REF:** The basics of pdf export ([77d3f04](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/77d3f04f650f922f03a95a62129ba2735b918026))
* **REF:** Using percentages for pdfmake tables ([9608a26](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/9608a260ff61a83bcb370cf33717af4125e1621e))
* Result vertically centered ([8ab630d](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/8ab630d13a61ccde91008fe536e56c2b35ebf58c))
* Rounding to two decimal places ([5d8e9cd](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/5d8e9cd813d5a413f3693d9b789679f6efde9731))
* Starting on layer features ([111ce35](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/111ce3526a24fe92e58094a35696ce0be2e7bffd))
* Style now in own css and js drawing box ([c93f744](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/c93f7448024c675393e272703c4b6ade16ff8b44))
* Visible Modal Div ([4491081](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/44910812c5e1c835098a4c87c0cd48bd6b513d49))
* Zoom disable now waits for map to load ([15ae5bf](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/15ae5bfa397fecbea0c0683c42a5a8d458ea64f0))


### Bug Fixes

* **REF:** fixed function name mispelling ([13d31af](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/13d31aff3a2868e2a5e7e40e44e84fc27cb464d3))

### [0.3.8](https://gitlab.com/ccrpc/qwebmapgl-templates/compare/v0.3.7...v0.3.8) (2021-03-19)


### Features

* **tip:** add choices for FY 24 and 25 ([056049a](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/056049aa5ad54d3c867fe68cc72b19d6d721483d)), closes [#5](https://gitlab.com/ccrpc/qwebmapgl-templates/issues/5)

### [0.3.7](https://gitlab.com/ccrpc/qwebmapgl-templates/compare/v0.3.6...v0.3.7) (2021-01-06)

### [0.3.6](https://gitlab.com/ccrpc/qwebmapgl-templates/compare/v0.3.5...v0.3.6) (2020-10-26)


### Features

* **table:** Remove change_agency from table. ([2c37df9](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/2c37df97c449bae1fdad59d901fcc5ce6b00157d)), closes [#4](https://gitlab.com/ccrpc/qwebmapgl-templates/issues/4)

### [0.3.5](https://gitlab.com/ccrpc/qwebmapgl-templates/compare/v0.3.4...v0.3.5) (2020-10-08)


### Bug Fixes

* **column:** Fix federal performance measure default ([08faba4](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/08faba45e9589a0c1e49620fa441f4af94e55cda)), closes [#3](https://gitlab.com/ccrpc/qwebmapgl-templates/issues/3)

### [0.3.4](https://gitlab.com/ccrpc/qwebmapgl-templates/compare/v0.3.3...v0.3.4) (2020-10-08)


### Features

* **index:** Add table-filter and gl-table-column for pm ([7c6ba68](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/7c6ba68b4d03f29b7a20f66c2ccc664ef46b4f40)), closes [#2](https://gitlab.com/ccrpc/qwebmapgl-templates/issues/2)

### [0.3.3](https://gitlab.com/ccrpc/qwebmapgl-templates/compare/v0.3.2...v0.3.3) (2020-08-13)

### 0.3.2 (2020-02-14)


### Features

* **dependencies:** update Mapbox GL and Web Map GL ([d6868e0](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/d6868e01a2445947d75e2c44e3fe0a4fc992f0a8)), closes [#1](https://gitlab.com/ccrpc/qwebmapgl-templates/issues/1)
* **dependency:** Upgrade mapboxgl and tip version. ([7322490](https://gitlab.com/ccrpc/qwebmapgl-templates/commit/7322490282e9951783e8f19100b02db1bb493fcf))

## [0.3.1] - 2019-08-30
- Use `@ccrpc/tip@0.4.2` to fix project history, memos, and presentations.

## [0.3.0] - 2019-08-29
- Use `@ccrpc/tip@0.4.1` to add project history, memos, and presentations.

## [0.2.2] - 2019-06-21
- Use `@ccrpc/webmapgl@0.14.0` to fix an issue with the popup width.

## [0.2.1] - 2019-06-03
- Added revision to data URLs to prevent unwanted caching.

## [0.2.0] - 2019-06-03
- Added Transportation Improvement Program template.
- Added download button to basic template.

## [0.1.0] - 2019-05-02
- Initial release
